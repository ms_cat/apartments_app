This is a simple web application for booking apartments
=

To start this project, please, clone this repository, open it into your IDE like Visual Studio Code, Webstorm etc.
--

* *In the project directory, you can run:*

### `npm start`

* *Runs the app in the development mode.*<br>
*Open [http://localhost:3000](http://localhost:3000) to view it in the browser.*


After the project was run you could find a list of apartments. Try to click on the row of apartment you like and you will route to the page of details about the current item. The button 'Book' is just a mock and doesn't have any functionality. The button 'Go to Apartments' will navigate you to the previous page.
