import React, { Component } from 'react';
import { apartmentsList } from '../helper/ApartmentsList';
import { convertToDate } from '../helper/service';
const uuidv = require('uuid/v1');

export default class Apartments extends Component {

  goTo = (id) => {
    this.props.history.push(`apartments/${id}`);
  }

  render() {
    const { apartments } = apartmentsList;
    return (
      <>
        {
          apartments
          .map( el => <>
            <div className="list-group">
              <button
              className="list-group-item list-group-item-action wrapper"
              onClick={() => this.goTo(el.uuid)}
              key={uuidv()}>
                <div className="col-sm-4" ><img className="image" src={el.image} alt="Apartment"></img></div>
                <div className="col-sm-4">{el.name}</div>
                <div className="col-sm-2"><b>{el.price}$</b> per night</div>
                <div className="col-sm-2">This apartment is available from: <b>{convertToDate(el.availableFrom)}</b></div>
              </button>
            </div>
          </>)
        }
      </>
    )
  }
}