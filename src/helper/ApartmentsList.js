const uuidv = require('uuid/v1');

export const apartmentsList = 
{
  "apartments": [
    {
      "uuid": uuidv(),
      "name": "Single attic room for one person only",
      "location": {
        "city":  "Kyiv",
        "street": "Kyivska",
        "house": 11,
      },
      "image": "https://amp.insider.com/images/5b3fbc21c8d6ed47008b45e0-750-563.jpg",
      "price": 30,
      "availableFrom": Date.now(),

    },
    {
      "uuid": uuidv(),
      "name": "Modern Houseboat/Large Roof Terrace",
      "location": {
        "city":  "Lviv",
        "street": "Lvivska",
        "house": 12,
      },
      "image": "https://techcrunch.com/wp-content/uploads/2019/03/blueground-apartment-2-2-2.jpg?w=730&crop=1",
      "price": 46,
      "availableFrom": Date.now(),

    },
    {
      "uuid": uuidv(),
      "name": "Private Luxurious house gardenview",
      "location": {
        "city":  "Lviv",
        "street": "Lvivska",
        "house": 12,
      },
      "image": "https://s3-ap-southeast-2.amazonaws.com/media.meritonsuites.com.au/wp-content/uploads/2017/06/26014016/meriton-1-770x501.jpg",
      "price": 55,
      "availableFrom": Date.now(),

    },
    {
      "uuid": uuidv(),
      "name": "Private Attic Studio/Roofterrace",
      "location": {
        "city":  "Lviv",
        "street": "Lvivska",
        "house": 12,
      },
      "image": "https://dynamicmedia.irvinecompany.com/is/image/content/dam/apartments/3-readytopublish/communities/orangecounty/irvine/parkplace/photography/PPIII-INT-MAY2018-LIVRM-A.tif?&wid=766&hei=560&qlt=60&crop=0,0,5202,3798&fit=stretch&iccEmbed=1&icc=AdobeRGB&fmt=pjpeg&pscan=auto",
      "price": 28,
      "availableFrom": Date.now(),

    },
  ]
};