import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import App from './App'
import Apartments from './Apartments/Apartments';
import Details from './Details/Details';
import NotFound from './NotFound/NotFound';

const routing = (
  <Router>
    <div>
      <Switch>
        <Route exact path="/" component={Apartments} />
        <Route path="/apartments/:id" component={Details} />
        <Route path="/apartments" component={Apartments} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))