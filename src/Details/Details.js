import React, { Component } from 'react';
import { apartmentsList } from '../helper/ApartmentsList';
import { convertToDate } from '../helper/service';

export default class Details extends Component {

  goBack = () => {
    this.props.history.goBack();
  }

  render() {
    const { match: { params: { id } }} = this.props;
    const { apartments } = apartmentsList;
    const item = apartments.filter((el) => el.uuid === id);

    return (
      <>
        <div className="jumbotron">
          <h1>{item.map(el => el.name)}</h1>
          <p className="lead">Address: 
            {
              item
              .map(el => <span>
                {el.location.city},
                {el.location.street} street/
                {el.location.house}
              </span>)
            }
          </p>
          <hr className="my-4"/>
          <div class="text-center">
            <img src={item.map(el => el.image)} class="rounded img" alt="Apartment"/>
          </div>
          <hr className="my-4"/>
          <p>
            <b>{item.map(el => el.price)}$</b> per night
          </p>
          <h4> Hurry up! Only one available date {item.map(el => convertToDate(el.availableFrom))} remains to book this apartment!</h4>
          <hr className="my-4"/>
          <p className="lead">
            <button type="button" className="btn btn-primary btn-lg margin">Book</button>
            <button className="btn btn-secondary btn-lg" type="button" onClick={() => this.goBack()}>Go to Appartments</button>
          </p>
        </div>
      </>
    )
  }
}